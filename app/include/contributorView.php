<!-- Included in index.php to show logged in users videos/playlists -->

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-lg-6">
		    <div class="panel panel-default">
		        <div class="panel-heading">
		        	<h3 class="panel-title">Mine videoer</h3>
		        </div>
	            <div class="panel-body">
	            	<?php
	            		require_once 'classes/video.php';
                  // Inlcude owner column in sql select statement to also show link to editing of video
	            		$video->createVideoListTable("SELECT id, owner, title, description, duration FROM video WHERE owner=? ORDER BY title", array ($user->getUID()));
	            	?>
	           	</div>
		    </div>
		</div>
		<div class="col-xs-12 col-lg-6">
		    <div class="panel panel-default">
		        <div class="panel-heading">
              <!-- Show form for creating new playlist -->
              <div class="container-fluid" style="float: right;margin-top:-9px;">
                <form class="form-inline" method="post" action="index.php">
                  <div class="form-group">
                    <label for="newPlaylistName">Name</label>
                    <input type="text" class="form-control" name="newPlaylistName" id="newPlaylistName" placeholder="Navn på spilleliste">
                  </div>
                  <button type="submit" class="btn btn-default">Opprett spilleliste</button>
                </form>
              </div>
		        	<h3 class="panel-title">Mine spillelister</h3>
		        </div>
	            <div class="panel-body"> 
                <?php
                  require_once ('classes/playlist.php');
                  // Include owner column in sql select to also show link to editing of playlist
                  $playlist->generatePlaylist ('SELECT id, owner, title, description FROM playlist WHERE owner=? ORDER BY title', array ($user->getUID())); 
                ?> 
	           	</div>
		    </div>
		</div>
	</div>
</div>