<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-lg-6">
		    <div class="panel panel-default">
		        <div class="panel-heading">
		        	<h3 class="panel-title">Videoer lagt ut av Lærer</h3>
		        </div>
	            <div class="panel-body">
	            	<?php
                   // Inlcude owner column in sql select statement to also show link to editing of video
                    require_once 'classes/video.php';
                    $video->createVideoListTable("SELECT id, owner, title, description, duration FROM video WHERE owner=? ORDER BY title", array ($user->getTeacherId()));
	              	?>
	           	</div>
		    </div>
		</div>
  </div>
</div>
