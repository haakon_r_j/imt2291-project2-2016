<?php
session_start();
require_once 'include/db.php';
require_once 'classes/user.php';

// Check if a user is logged in and a file has been uploaded
if ($user->isLoggedIn()&&isset($_POST['title'])) {
    // Add entry to database
    $sql = "INSERT INTO video (owner, title, description, filename, filepath, mimetype, duration) VALUES (?, ?, ?, ?, ?, ?, ?)";
    $sth = $db->prepare($sql);
    $sth->execute (array ($user->getUID(), $_POST['title'], $_POST['description'], $_POST['filename'], "", $_POST['mime'], $_POST['duration']));

    // Get id of last insertion, this will be part of the filename
    $id = $db->lastInsertId();

    // Move the file and give it a name where the id is part of the filename
    rename ($_POST['filepath'], "uploads/video_".$id);

    // Get the thumbnail image and insert it into the database
    $thumb = file_get_contents('uploads/temp/'.$_POST['thumb']);
    $sql = "INSERT INTO videoaddons (vid, mime, content) VALUES (?, 'image/jpeg', ?)";
    $sth = $db->prepare($sql);
    $sth->execute (array ($id, $thumb));

    // Remove all (four) thumbnail images from the temporary folder
    @unlink ($_POST['filepath'].'_001.jpg');
    @unlink ($_POST['filepath'].'_002.jpg');
    @unlink ($_POST['filepath'].'_003.jpg');
    @unlink ($_POST['filepath'].'_004.jpg');

    $uploadSuccess = true;
}

?>
<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Velkommen til undervisningsvideor på nett</title>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- build:css styles/vendor.css -->
    <!-- bower:css -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.css" />
    <!-- endbower -->
    <!-- endbuild -->

    <!-- build:css styles/main.css -->
    <link rel="stylesheet" href="styles/main.css">
    <!-- endbuild -->
    <script>
        var menuItemSelected = "upload";
    </script>

    
  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <?php 
        require_once 'include/topMenu.php'; 

        if (isset($uploadSuccess)) { // En video er lastet opp, gi brukeren beskjed ?>
            <div class="alert alert-success" role="alert">
                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                <span class="sr-only">Suksess:</span>
                En ny video er lastet opp.
                <!-- TODO : Her vil vi sende brukeren videre til redigering av videoen, legge til tekstspor -->
            </div> <?php
        }

        if ($user->isLoggedIn()) {  // Only show file upload form if user is logger in
            require_once 'include/upload.html';
        }

        require_once 'include/bottomScriptIncludes.html';

        if ($user->isLoggedIn()) {  
            // JavaScript handling actuall uploading of the video and 
            // displaying of thumbnails.
            require_once 'scripts/upload.js';
        }

    ?>

  </body>
</html>
