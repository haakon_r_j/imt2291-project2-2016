<?php
require_once 'classes/playlist.php';    // Uses the playlist object to perform playlist operations

/**
 * This class is used for assorted manipulations of videos/lists of videos.
 *
 * @author Øivind Kolloen
 *
 */
class Video {
	var $db;
    var $id = -1;
    var $mine = false;

	/**
	 * Constructor, saves a reference to the database.
	 * if an id is also passed then the GET/POST variables will be checked for
	 * updates to that particular video
	 */
	function Video ($db, $id=null) {
		$this->db = $db;
		if ($id!==null) {
			$this->setID($id);
			if (isset($_POST['addAnnotation'])&&$this->mine) {
				$this->addAnnotation ();
			}
		}
	}

    /**
     * This instance of the video object is used to represent a particular video
     * Use this method to set the id of the video this object is representing.
     * Will also check to see if the currently logged in user owns this video.
     *
     * @param id the id of the video this object is representing.
     */
    function setID ($id) {
        global $user;
    	if ($this->id!==-1)
    		return;
        $this->id = $id;
        $sql = 'SELECT owner FROM video WHERE id=?';
        $sth = $this->db->prepare ($sql);
        $sth->execute (array($id));
        if ($row=$sth->fetch()) {
            $this->mine = ($row['owner']==$user->getUID());
        }
    }

    /**
     * Based on the passed in sql statement and parameters this method
     * will generate a table that presents videos for the user to select
     * from.
     * If the field owner exists in the query it will be assumed that
     * a shortcut to editing of the video should be created.
     *
     * @param sql the sql statement to be executed, the result will be
     * used to create the contents of the table
     * @param params the parameters to be passed when executing the sql
     * statement
     *
     */
	function createVideoListTable ($sql, $params) {
        global $user;?>
		<table id="videos" class="table table-striped table-hover">
            <thead>
                </th><th style="width:200px">Tittel</th><th>Beskrivelse</th>
            </thead>
            <tbody> <?php
            	$sth = $this->db->prepare ($sql);
            	$sth->execute ($params);
            	while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
                    $dt = "";
                    if ($row['duration']!==null) {  // Some very early videos did not have duration
                        $t = round($row['duration']);   // Get rid of thousands part
                        $t = sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
                        // The css definition is in styles/main.css
                        $dt = '<span class="video-duration">'.$t.'</span>';
                    }
                    // Show title and thumbnail
                    echo "<tr><td><b><a href='#home?video={$row['id']}'>{$row['title']}</b><br/><span style='position: relative'><img src='api/thumbnail.php?id={$row['id']}' class='img-responsive img-rounded'/>$dt</span></a></td><td data-id='{$row['id']}'>";
                    if (isset ($row['owner'])) {
                        echo "<a href='#editVideo?video={$row['id']}' title='Rediger videoen'><span style='float: right;' class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>";
                    }
                	echo "{$row['description']}</td></tr>";
            	} ?>
            </tbody>
        </table> <?php
	}

    /**
     * This method is used to create the video tag with source and track.
     * mp4 video is assumed, this might change in the future.
     * All  text tracks added to this video is added.
     * The javascript file scripts/video.js is added, this extracts
     * the information from the text default text track and adds it
     * to the right side of the video.
     * If the currently logged in user owns this video then the option
     * for adding it to a playlist is added.
     * Lastly an entry is added to the log to indicate that this video has
     * been viewed.
     *
     * @param id the id of the video to display
     */
    function createVideoTag ($id) {
        global $editing, $user, $playlist;
        $this->setID($id);
        echo '<video class="img-responsive" controls style="margin: auto">';
        echo '<source src="uploads/video_'.$id.'" type="video/mp4">';
        $sql = "SELECT id, extras from videoaddons WHERE vid=? AND mime='text/vtt'";
        $sth = $this->db->prepare($sql);
        $sth->execute (array($this->id));
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {  // Add all text tracks
        	$track = json_decode($row['extras']);
        	echo '<track ';
        	if ($track->default)
        		echo 'default ';
  	     	echo "kind='{$track->kind}' label='{$track->label}' srclang='{$track->srclang}' src='api/getTrack.php?id={$row['id']}'/>";
        }
        echo '</video>';
        // Add javascript for text track handling
        echo '<script src="scripts/video.js"></script>';
        if ($this->mine&&!$editing) {
            echo '<p class="bg-info">';
        }

        if ($this->mine) {  // If logged in user is owner, add option for adding to playlist
            $playlist->createAddToPlaylist();
        }
        if ($this->mine&&!$editing) {   // If owner and not editing mode, add link to edit this video
            echo '<a class="btn btn-default" href="#editVideo?video='.$id.'">Rediger video</a> ';
        }

        // Add an entry to the log
        $sql = 'INSERT INTO log (uid, vid) VALUES (?, ?)';
        $sth = $this->db->prepare ($sql);
        $sth->execute (array ($user->getUID(), $id));
    }

		//finner studenter som har sett på videoer lærer har lagt ut
		function videoSeenByStudent() {
			if(isset($_SESSION['uid'])){
				$id = $_SESSION['uid'];
				$sql = "SELECT admin FROM user WHERE id = '$id'";
				$sth = $this->db->prepare ($sql);
				$sth->execute();
				$result = $sth->fetch(PDO::FETCH_ASSOC);
				if($result['admin'] == 'y') {	// Bare admin (lærer) skal kunne se hvilke studenter som har sett videoer, fungerer ikke!
				?>	<div class="container-fluid">
				    <div class="row">
					            	<?php
											  $sql = "SELECT DISTINCT student.id, studentNr, teacherId FROM student INNER JOIN log
												ON student.id = log.uid
												WHERE student.teacherId = ?";
				                   // Inlcude owner column in sql select statement to also show link to editing of video
													 	$this->createStudentWatchList("SELECT DISTINCT student.id, studentNr, user.uname FROM student INNER JOIN log
														ON student.id = log.uid
														INNER JOIN user
														ON student.id = user.id
														WHERE student.teacherId = ?", array ($id));
				}		//	end ifAdministrator, fungerer ikke
		}
}
    //Lager en liste over studenter som har sett videoen lagt ut
		//av sin lærer
    function createStudentWatchList($sql, $params) { ?>
				<?php
						$sth = $this->db->prepare ($sql);
				    $sth->execute ($params);
						?><div class="panel panel-default">
								<div class="panel-heading"><b class="panel-title"><h3>Studenter som har sett din video</h3></b></div><?php
					  if ($sth->rowCount()==0){ ?>
								<h5 style="padding-left: 15px"><b> <?php echo "Ingen av dine studenter har sett denne videoen";?> </b></h5>
						<?php } ?>
								<div class="panel-heading">
												<?php	while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {?>
	  											<li style="padding-left: 10px"><b>
														  StudentId: <?php echo $row['id']; ?>
														  &nbsp;&nbsp;&nbsp;&nbsp;
									  					Brukernavn: <?php echo $row['uname']; ?>
															&nbsp;&nbsp;&nbsp;&nbsp;
								    					Studentnummer: <?php echo $row['studentNr'];?></b></li>
													<?php } ?>
										</div>
								</div>
					<?php
		}
    /**
     * This functions displays the add annotations (text track) form
     * The form is defined in include/uploadAnnotationsForm.php
     */
    function createUploadAnnotationsForm () {
    	$videoid = $this->id;
    	require_once 'include/uploadAnnotationsForm.php';
    }


		function createUploadTxtForm () {
    	$videoid = $this->id;
			require_once "../app/test.php"; // tilkaller test, brukte egentlig txtform.php men fikk den ikkje til å fungere med å legge inn i subittles (content)
    }

    /**
     * This method is called if the user has uploaded an annotation (text track)
     * for this video.
     */
    function addAnnotation () {
    	$sql = 'INSERT INTO videoaddons (vid, mime, extras, content) VALUES (?, "text/vtt", ?, ?)';
        // Information about the text track is stored as json encoded data
    	$extras = json_encode(array ('default'=>isset($_POST['default']), 'kind'=>$_POST['kind'], 'label'=>$_POST['label'], 'srclang'=>$_POST['srclang']));
    	$content = "";
    	if (is_uploaded_file($_FILES['file']['tmp_name'])) {   // A file has been uploaded
    		$content = file_get_contents($_FILES['file']['tmp_name']);
    	}
    	$sth = $this->db->prepare ($sql);
        // Store the text track in the database
    	$sth->execute (array ($_GET['video'], $extras, $content));
    }
}
		function deleteTxt() { // ikkje sjekket om virker.

			echo "<a href='deleteTxt.php?id={$row['id']}'/>";

		}

if (isset($_GET['video']))      // We are on a page that shows a video
	$video = new Video($db, $_GET['video']);
else
	$video = new Video($db);   // The object is to be used for other tasks than displaying a video
