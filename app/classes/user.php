<?php
/**
 * The User class handles user login/logout and maintaining of login status.
 * The class accepts a database handle in the constructor and assumes there is
 * a table called users with the columns : id, email, pwd, givenname and surename.
 * For creating new users it is also assumed that the id field is auto incrementet
 * and that the email field has an index that enforces unique email (username).
 *
 * @author imt2291
 *
 */
class User {
	var $uid = -1;
	var $name = NULL;
	var $db;
	var $alert = "";
	var $sessionAlert = "";
	var $admin = false;

	/**
	 * The constructor accepts an object of PDO that contains the database connection.
	 * It is also assumed that the session is started before this file get included.
	 * Also note that $_SESSION, $_GET and $_POST needs to be of type superglobal.
	 *
	 * The constructor checks for a $_GET variable of logout, if it exists the user
	 * is logget out.
	 *
	 * If $_POST['email'] is set a logon is attempted. The password should then be
	 * in $_POST['password'].
	 *
	 * if neither $_GET['logout'] nor $_POST['email'] is set then $_SESSION['uid']
	 * is checked. If this is set the user details (name of the user) is retrieved
	 * for the database. If no user with id $_SESSION['uid'] exists the session
	 * variable is cleared.
	 *
	 * @param PDO $db
	 */
	function User ($db) {
		$this->db = $db;	// Store a refence to the database connection, not needed now, but maybe in the future
		if (!isset($_SESSION['uid'])&&isset($_COOKIE['persistant'])) {	// No active session but a persistant login cookie was found
			list($uid, $identifier, $token) = explode (';', $_COOKIE['persistant']);
			$sql = 'SELECT token FROM persistantLogin WHERE uid=? AND identifier=?';
			$sth = $this->db->prepare ($sql);
			$sth->execute(array ($uid, $identifier));
			if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {	// uid and identifier found
				if ($row['token']===$token) {			// Correct token found, log the user in
					$_SESSION['uid'] = $uid;
					$token = md5($this->uid.time());
					$sql = "UPDATE persistantLogin set token=? WHERE uid=? AND identifier=?";
					$sth = $this->db->prepare ($sql);
					$sth->execute (array ($token, $uid, $identifier));
					setcookie("persistant", $uid.";$identifier;$token", time()+60*60*24*30);
				} else { // A theft has probably occured
					$sql = "DELETE FROM persistantLogin WHERE uid=?";
					$sth = $this->db->prepare ($sql);
					$sth->execute (array($uid));	// Delete all users persistant sessions
					setcookie("persistant", "", time());
					$this->sessionAlert = '<div class="alert alert-danger" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Warning:</span>
  Found a persistant login cookie but with outdated information. This might mean that someone has breached your security. All logged in sessions are terminated!
</div>';
				}
			}
		}
		if (isset($_GET['logout'])) {	// Logging out
			unset ($_SESSION['uid']);	// Clear the session variable
			setcookie("persistant", "", time());
		} else if (isset($_POST['email'])) {	// Logging in
			$sql = "SELECT id, pwd, fullName, admin FROM user WHERE uname=?";
			$sth = $db->prepare($sql);
			$sth->execute (array ($_POST['email']));	// Get user info from the database
			if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {	// If user exists
				if (password_verify ($_POST['password'], $row['pwd'])) {	// If correct password
					$_SESSION['uid'] = $row['id'];		// Store user id in session variable
					$this->uid = $row['id'];			// Store user id in object
					$this->name = $row['fullName'];
					if (isset($_POST['remember']))
						$this->rememberLogin();
					$this->admin = $row['admin']==='y';
				} else {	// Bad password
					$this->unknownUser = 'Uknown username/password';
					// Note, never say bad password, then you confirm the user exists
				}
			} else {		// Unknow user
				$this->unknownUser = 'Uknown username/password';
				// Same as for bad password
			}
		} else if (isset($_SESSION['uid'])) {	// A user is logged in
			$sql = "SELECT fullName, admin FROM user WHERE id=?";
			$sth = $db->prepare($sql);
			$sth->execute (array ($_SESSION['uid']));	// Find user information from the database
			if ($row = $sth->fetch()) {					// User found
				$this->uid = $_SESSION['uid'];			// Store user id in object
				$this->name = $row['fullName'];
				$this->admin = $row['admin']==='y';
			} else {									// No such user
				unset ($_SESSION['uid']);				// Remove user id from session
			}
		}
	}

	/**
	 * This method is used to initialize a persistant login instance.
	 * Stores the uid, identifier and token in the database and
	 * in a cookie on the users browser.
	 */
	function rememberLogin () {
		$identifier = md5($this->name.time());
		$token = md5($this->uid.time());
		$sql = "INSERT INTO persistantLogin (uid, identifier, token) VALUES (?, ?, ?)";
		$sth = $this->db->prepare ($sql);
		$sth->execute (array ($this->uid, $identifier, $token));
		setcookie("persistant", $this->uid.";$identifier;$token", time()+60*60*24*30);
	}

	/**
	 * Use this function to get the user id for the logged in user.
	 * Returns -1 if no user is logged in, or the id of the logged in user.
	 *
	 * @return long integer with user id, or -1 if no user is logged in.
	 */
	function getUID() {
		return $this->uid;
	}

	/**
	 * Use this function to get the name of the user.
	 * Returns NULL if no user is logged in, an array with given name and
	 * surename of a user is logged in.
	 *
	 * @return array containing first and last name of user
	 */
	function getName() {
		return $this->name;
	}

	/**
	 * This method returns true if a user is logged in, false if no
	 * user is logged in.
	 *
	 * @return boolean value of true if a user is logged in, false if no user is logged in.
	 */
	function isLoggedIn() {
		return ($this->uid > -1);	// return true if userid > -1
	}

	/**
	 * This method returns true if a user is logged in and is an administrator.
	 * If not logged on or not administrator false is returned
	 *
	 * @return boolean value of true if logged on user is admin
	 */
	function isAdministrator() {
		return ($this->admin);	// return true for admin
	}


	/**
	 * This method returns HTML kode for the login form.
	 * Note that any login attempt detected by the constructor will affect
	 * the outcome of this method. If a successfull login has been performed
	 * this method return an empty string.
	 * If a failed login has been detected an alert box will be shown informing
	 * the user of this fact. The user name will also be filled in with information
	 * from the failed attempt.
	 *
	 * This method also uses the calling script as recipient in the action attribute
	 * of the form.
	 *
	 * @return string with login form or blank string if already logged in.
	 */
	function getLoginForm () {
		if ($this->isLoggedIn())	// User is logged in
			return;					// Do not insert anything.
		$email = 'value=""';
		if (isset($this->unknownUser)) {	// If failed login
			// Set alert and email to be used in the form
			$this->alert = '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> '.$this->unknownUser.'</div>';
			$email = "value='{$_POST['email']}' ";
		}
		require_once "user.loginform.inc.php";
	}

	/**
	 * Method used to add a user to the database. Takes username, password, first and
	 * last name as parameters and attempts to add the user to the database.
	 *
	 * On success an array with the element 'success' is returned. If it
	 * failed (probably because the username was taken) an array with
	 * two items is return, 'error' is set and 'description' gives the reason
	 * for the failure.
	 *
	 * @param string $uname the username of the new user
	 * @param string $pwd the password for the new user
	 * @param string $givenname the first name of the new user
	 * @param string $surename the last name of the new user
	 * @return array that indicates failure or success.
	 */

function addUser ($uname, $pwd, $fullName, $studNr) {
	//hvis addUser ble tilkalt med "addStudent og studNr ikke har lengde = 6 return error"
	if(isset($_POST['addStudent']) && strlen($studNr) != 6) {
			return (array ('error1'=>'error1', 'description'=>'Studentnummer må være 6 siffer'));
	}
	//sjekker om gitt $studNr allerede eksisterer
  else if (isset($_POST['addStudent']) && strlen($studNr) == 6){
			$sql = "SELECT studentNr FROM student WHERE studentNr = '$studNr'";
			$sth = $this->db->prepare ($sql);
			$sth->execute();
			$result = $sth->fetch(PDO::FETCH_ASSOC);
			if(isset($result['studentNr'])){
					return (array ('error2'=>'error2', 'description'=>'Studentnummer finnes allerede'));
			}
	}
  //sjekker om gitt $uname (epost) allerede eksisterer
	$sql = "SELECT uname FROM user WHERE uname = '$uname'";
	$sth = $this->db->prepare ($sql);
	$sth->execute();
	$result = $sth->fetch(PDO::FETCH_ASSOC);
	if(isset($result['uname']))  {
			return (array ('error3'=>'error3', 'description'=>'Epost finnes allerede'));
	}

	//legger inn data i i bruker
	$sql = "INSERT INTO user (uname, pwd, fullName) VALUES (?, ?, ?)";
	$sth = $this->db->prepare ($sql);
	$sth->execute (array ($uname, password_hash($pwd, PASSWORD_DEFAULT), $fullName));

	//kaller på addStudent for å
	//legge til student data, hvis bruker er student
	if(isset($_POST['addStudent'])){
		$this->addStudent($studNr, $uname);
	}

	if ($sth->rowCount()==0)
			return (array ('error'=>'error', 'description'=>'email address already registered'));
		return (array ('success'=>'success'));
}

function addStudent($studNr, $uname) {
	//henter ut id fra user
	$sql = "SELECT id FROM user WHERE uname = '$uname'";
	$sth = $this->db->prepare ($sql);
	$sth->execute();
	$result = $sth->fetch(PDO::FETCH_ASSOC);

		if (isset($_SESSION['uid'])){
		$teacherId = $_SESSION['uid']; //henter id fra admin/teacher
		$id = $result['id']; //henter bruker id

		//Legger inn ny student
		$sql = "INSERT INTO student (id, studentNr, teacherId)
   				VALUES ('$id', '$studNr', '$teacherId')";
		$sth = $this->db->prepare ($sql);
  	$sth->execute();
	}
}

//Sjekker om pålogget bruker er student
function student() {
	$id = $_SESSION['uid'];
	$sql = "SELECT id FROM student WHERE id = '$id'";
	$sth = $this->db->prepare ($sql);
	$sth->execute();
	$result = $sth->fetch(PDO::FETCH_ASSOC);
	if(isset($result['id'])){
	  return true;
  }
	else return false;
}
//finner lærer id til student
function getTeacherId() {
	if(isset($_SESSION['uid'])) {
	$id = $_SESSION['uid'];
	//finner lærer id til pålogget student
	$sql = "SELECT teacherId FROM student WHERE id = '$id'";
	$sth = $this->db->prepare ($sql);
	$sth->execute();
	$result = $sth->fetch(PDO::FETCH_ASSOC);
  $teacherId = $result['teacherId'];

  return $teacherId;
	}
}

	/**
	 * Method used to remove a user from the user table. Takes userid
	 * as a parameter and attempts to remove the user from the database.
	 * Will also remove any entries in the persistant login table.
	 *
	 * On success an array with the element 'success' is returned. If it
	 * failed (probably because the userid doesn't exist) an array with
	 * two items is return, 'error' is set and 'description' gives the reason
	 * for the failure.
	 *
	 * @param string $id the id of the user to delete
	 * @return array that indicates failure or success.
	 */

	function removeUser ($id) {
		//Sletter bruker/Student med sendt med id
		$sql = 'DELETE FROM persistantLogin WHERE uid=?';
		$sth = $this->db->prepare ($sql);
	  $sth->execute (array ($id));			// Delete from persistant login

		$sql = 'DELETE FROM student WHERE id=?';
	  $sth = $this->db->prepare ($sql);
	  $sth->execute (array ($id));			// Delete from student table

	  $sql = 'DELETE FROM user WHERE id=?';
    $sth = $this->db->prepare ($sql);
    $sth->execute (array ($id));			// Delete from user table
    if ($sth->rowCount()==0)
	  	return (array ('error'=>'error', 'description'=>$sth->errorInfo()));
	  return (array ('success'=>'success'));
	}

/*En funksjon med et html skjema for å legge til student eller bruker, anhengig av hvilken
knapp som tilkalte funksonen. Blir bukt i admin.php*/
function addUserHTML() {
?>
	<div class="container-fluid">
			<div class="panel panel-default">
				<?php if(isset($_POST['addStudentHTML'])){?>
					<div class="panel-heading"><h3 class="panel-title">Legg til en ny student</h3></div>
				<?php } else { ?> <div class="panel-heading"><h3 class="panel-title">Legg til en ny bruker</h3></div> <?php } ?>
					<div class="panel-body" style="margin-top: 10px;">
							<form method="post" action="admin.php">
									<div class="row">
											<div class="col-xs-4">
													<div style="margin-bottom: 25px" class="input-group">
															<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
															<input type="email" class="form-control" name="new-email" placeholder="E-post adresse" required>
													</div>
											</div>
											<div class="col-xs-4">
													<div style="margin-bottom: 25px" class="input-group">
															<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
															<input type="password" class="form-control" name="new-password" placeholder="Passord" required>
													</div>
											</div>
											<div class="col-xs-4">
													<div style="margin-bottom: 25px" class="input-group">
															<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
															<input type="text" class="form-control" name="new-name" placeholder="Fullt navn" required>
													</div>
											</div>
										 </div>
									<?php //Hvis "Legg til student" ble trykket på, legges det til et ekstra element i skjema
									 	if(isset($_POST['addStudentHTML'])){?>
										 <div class="row">
											 <div class="col-xs-3">
													 <div style="margin-bottom: 25px" class="input-group">
															 <span class="input-group-addon"><i class="glyphicon glyphicon-check"></i></span>
															 <input type="text" class="form-control" name="new-studNr" minlenght="6" maxlength="6" placeholder="studentnummer (6 siffer)">
													 </div>
											 </div>
										 </div>
									   <div style="margin-bottom: 25px" class="input-group">
										 	<input type="submit" name="addStudent" value="Bekreft" class="btn btn-primary"/>
										</div>
									 <?php } else { ?>
									  <div style="margin-bottom: 25px" class="input-group">
										 <input type="submit" name="addUser" value="Bekreft" class="btn btn-primary"/>
									 	</div>
										<?php }	?>
									</form>
								</div>
							</div>
					 	</div>
					<?php
				}
}

// Create an object of the User class, this also makes sure the constructor is called.
$user = new User($db);?>
