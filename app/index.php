<?php
// Main page, serve three functions :
// Show newly uploaded videos and newly created playlist to users who are not logged in
// Show users videos and playlists to users who are logged in
// Show videos (if selected through playlist or from list of videos)
// TODO: Ability to search for videos
session_start();
require_once 'include/db.php';
require_once 'classes/user.php';
?>
<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Velkommen til undervisningsvideor på nett</title>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- build:css styles/vendor.css -->
    <!-- bower:css -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.css" />
    <!-- endbower -->
    <!-- endbuild -->

    <!-- build:css styles/main.css -->
    <link rel="stylesheet" href="styles/main.css">
    <!-- endbuild -->

    <!-- Trengs denne her? Har allerede inkludert jquery i bottomScriptIncludes som er inkludert lenger nede i denne filen -->
    <script src="../bower_components/jquery/dist/jquery.js"></script>

  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div id="hovedElement"> <!-- Div-elementet som vil inneholde alle sidene man bytter om på -->
      <?php
        require_once 'include/topMenu.php';

        if (isset($_GET['video'])) {    // A video is selected
            include_once ('include/viewVideo.php');
        } else if ($user->isLoggedIn()) {   // Show users videos/playlists
            include_once ('include/contributorView.php');
            //sjekker om bruker er student
            if($user->student()) {
              //legger til videoliste fra studentens lærer
              require_once ('include/studentView.php');
              echo 'Hei';
            }

        } else { // Show newly uploaded videos/created playlists
            include_once ('include/newVideosView.php');
        }

        require_once 'include/bottomScriptIncludes.html';
        if (!isset($_GET['video'])) { ?>
            <script src="../bower_components/dynatable/jquery.dynatable.js"></script>
            <script>
                $(function () {     // Make video lists/playlists searchable, sortable, pageable.
                    $('#videos').dynatable();
                    $('#playlists').dynatable();
                });
            </script> <?php
        }
      ?>
    </div>

  </body>
</html>
