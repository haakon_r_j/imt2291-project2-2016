<script>
// Included in upload.php, handles uploading of the video file
// and displaying of thumbnails
$(function() {
	$('#progress').hide();		// Skjul progress bar
	$('#uploadSuccess').hide();	// og upload success når siden vises
	$('#thumbs').hide(); 		// Skjul thumbnails, vises når en fil er lastet opp
	$('#processing').hide();
	$('[role="alert"]').fadeOut(5000);	// Fade ut eventuell beskjed om suksess for filopplasting
	$('#file').on('change', function (event) {	// Når brukeren har valgt en fil
		if (event.target.files.length==1) {		// Tillat kun en og en files
			$('#uploadFile').val(this.value);	// Vis filnavnet som er valgt
			$('#progress')[0].value = 0;		// Vi har ikke begynt opplastingen enda
			$('#progress').show();				// Vis frem progress baren
			var file = event.target.files[0];	// Litt kortere enn å referere til elementet i arrayen hver gang vil trenger informasjon om filen.
			$('#filename').val(file.name);
			$('#mimetype').val(file.type);
			$('#filesize').val(file.size);
			var xhr = new XMLHttpRequest();		// Overfør filen via AJAX
			xhr.open("POST", 'api/receiveFile.php', true);		// Hvilket skript skal ta i mot filen
			xhr.upload.onprogress = function (event) {		// Motta beskjed underveis i overføringen
				if (event.lengthComputable) {		// Har overføringen startet
					// Her kan en eventuelt også oppdatere med antall bytes overført/totalt antall bytes som skal overføres/estimert tid som gjennstår
					var complete = (event.loaded/event.total*100|0);
					$('#progress')[0].value = complete;		// Oppdater progress baren
					if (event.loaded===event.total) {
						$('#processing').show();
					}
				}
			}
			xhr.onload = function(e) {		// Kalles når overføringer er ferdig
				if (this.status == 200) {	// Dersom alt er OK
					var data = JSON.parse(this.responseText);
					if (data.ok) {
						for (var i=0; i<data.thumbs.length; i++) {
							$('#thumb'+i).attr('src', 'uploads/temp/'+data.thumbs[i]);
							$('#thumb'+i).prev().attr('value', data.thumbs[i]);
						}
						$('#thumbs').show();
						$('#submitbutton').removeAttr("disabled");
						$('#filepath').val(data.filename);
						$('#duration').val(data.duration);
						$('#upload').hide();
						$('#processing').hide();
						$('#progress').hide();		// Skjul progress baren
						$('#uploadSuccess').hide();	// En liten fiks for fadeOut (nødvendig dersom en skal overføre flere filer)
						$('#uploadSuccess').show();	// Vis beskjed om suksess
						$('#uploadSuccess').fadeOut(4000);	// Fade ut denne
					}
				}
			}
			xhr.send(file);		// Send filen
		}
	});
});
</script>
