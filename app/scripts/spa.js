/* Script for å håndtere overgangen til single page application */

/*
  Funksjon for å identifisere URL variable, hentet herifra:
  https://ericholmes.ca/retreive-get-variables-in-javascript-from-a-url-string/
*/
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function locationHashChanged() {

  console.log("Hash changed!");

  var varURL = location.hash; //  Holder på hash'en til URLen
  console.log(varURL);

  $('#hovedElement').empty(); // Tømmer alltid hovedelementet før ny side lastes inn

  (location.hash === '') && (location.hash = "#home");  // Første gang siden blir lastet inn
  (location.hash === "#home") && $('#hovedElement').load('index.php');
  (location.hash === "#login") && $('#hovedElement').load('login.php');
  (location.hash === "#logout") && $('#hovedElement').load('logout.php?logout=true');
  (location.hash === "#admin") &&  $('#hovedElement').load('admin.php');
  (location.hash === "#upload") && $('#hovedElement').load('upload.php');

  if(varURL.indexOf('video') !== -1) { // Hvis noe skal gjøres med en video sendes det med en videoID
    var videoID = getUrlVars()['video'];  //  Lagrer videoID
    (varURL.indexOf('#editVideo') !== -1) && $('#hovedElement').load('editVideo.php?video=' + videoID); // Hvis det skal redigeres
    (varURL.indexOf('#home') !== -1) && $('#hovedElement').load('index.php?video=' + videoID);  // Hvis video skal vises
  }

  if(varURL.indexOf('id') !== -1) { //  Hvis noe skal gjøres med en spilleliste sendes det med en id
    var playlistID = getUrlVars()['id'];
    (varURL.indexOf('#editPlaylist') !== -1) && $('#hovedElement').load('editPlaylist.php?id=' + playlistID);
    (varURL.indexOf('#playlist') !== -1) && $('#hovedElement').load('playlist.php?id=' + playlistID);
  }

}

window.onhashchange = locationHashChanged;  // Kjører funksjonen hver gang det skjer en forandring i hash'en

$(window).load(function() { // Finner riktig side når den blir lastet inn
  locationHashChanged();
});
