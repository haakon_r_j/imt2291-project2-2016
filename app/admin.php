<?php
session_start();
require_once 'include/db.php';
require_once 'classes/user.php';
?>
<!doctype html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Velkommen til undervisningsvideor på nett</title>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- build:css styles/vendor.css -->
    <!-- bower:css -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.css" />
    <!-- endbower -->
    <!-- endbuild -->

    <!-- build:css styles/main.css -->
    <link rel="stylesheet" href="styles/main.css">
    <!-- endbuild -->

    <script>
        var menuItemSelected = "admin";
    </script>

  </head>
  <body>
    <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php  if ($user->isAdministrator()) {     // No other users should be able to do this
  require_once 'include/topMenu.php';     ?>
    <!--To inputknapper som brukes for å tilkalle skjema for å legge inn student eller bruker -->
    <div class="container-fluid">
        <div class="panel panel-default">
          <div class="panel-heading"><h3 class="panel-title">Legg til en ny bruker/Student</h3></div>
             <div class="panel-body" style="margin-top: 10px;">
                 <form method="post" action="admin.php">
                   <div class="row">
                       <div class="col-xs-1">
                         <div style="margin-bottom: 25px" class="input-group">
                            <input type="submit" name="addUserHTML" value="Legg til bruker" class="btn btn-primary"/>
                          </div>
                        </div>
                        <div class="col-xs-1">
                          <div style="margin-bottom: 25px" class="input-group">
                            <input type="submit" name="addStudentHTML" value="Legg til student" class="btn btn-primary"/>
                          </div>
                      </div>
                    </div>
                  </form>
              </div>
          </div>
      </div>
    <?php

    if (isset($_POST['addUserHTML']) || isset($_POST['addStudentHTML']))$user->addUserHTML(); //henter skjema for å legge til
    //bruker eller student. Funksjonen blir tilkalt fra User.php

    if (isset($_POST['addUser'])) { // Add a user
        $res = $user->addUser($_POST['new-email'], $_POST['new-password'], $_POST['new-name'], '');
          if (isset($res['success'])) { // Let the admin know user was added ?>
              <div class="alert alert-success" role="alert">
                  <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                  <span class="sr-only">Bruker opprettet:</span>
                  Ny bruker er opprettet og kan ta i bruk systemet
                  </div> <?php
          }
          else if (isset($res['success'])) { // Let the admin know user was added ?>
              <div class="alert alert-success" role="alert">
                  <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                  <span class="sr-only">Bruker opprettet:</span>
                  Ny bruker er opprettet og kan ta i bruk systemet
              </div> <?php
          }
          else { // error melding, Epost eksisterer fra før  ?>
                <div class="alert alert-danger" role="alert">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <span class="sr-only">Feil:</span>
                  Epost eksisterer allerede
               </div> <?php
               //skjører riktig skjema for å legge til bruker
               $user->addUserHTML();
            }
          }
          else if (isset($_POST['addStudent'])) { // Add a student
            $res = $user->addUser($_POST['new-email'], $_POST['new-password'], $_POST['new-name'], $_POST['new-studNr']);
              if (isset($res['success'])) { // Let the admin know student was added ?>
                <div class="alert alert-success" role="alert">
                  <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                  <span class="sr-only">student er opprettet:</span>
                  Ny student er opprettet og kan ta i bruk systemet
              </div> <?php
          }
          else if (isset($res['error1'])) { // error melding, "for få siffer på studentnummer" ?>
            <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Feil:</span>
                Studentnummer må inneholde 6 siffer
            </div> <?php
            //skjører riktig skjema for å legge til student
            //trenger å ha 'addStudentId' = 1 for å få med skjema forstudentnummer
            $_POST['addStudentHTML'] = 1; $user->addUserHTML();
          }
          else { // error melding, epost eller studentnummer eksisterer allerede ?>
             <div class="alert alert-danger" role="alert">
                 <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                 <span class="sr-only">Feil:</span>
                  Studentnummer/Epost eksisterer allerede
            </div> <?php
            //skjører riktig skjema for å legge til student
            //trenger å ha 'addStudentId' = 1 for å få med skjema forstudentnummer
            $_POST['addStudentHTML'] = 1; $user->addUserHTML();
          }

      }
      else if (isset($_GET['deleteUser'])) { // remove a user
          $res = $user->removeUser($_GET['deleteUser']);
          if (isset($res['success'])) { // Let the admin know user was added ?>
              <div class="alert alert-success" role="alert">
                  <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                  <span class="sr-only">Bruker slettet:</span>
                  Brukeren er slettet fra systemet
                </div> <?php
                } else { // Let the admin know something went wrong ?>
                  <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Feil:</span>
                    Kunne ikke slette brukeren?????
                  </div> <?php
          }
      }
?>
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Liste over brukere av systemet</h3></div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="userTable" class="table table-striped table-hover">
                                <thead>
                                    <th>Username</th><th width="60%">Full name</th><th style="width:30px"></th>
                                </thead>
                                <tbody>
<?php
    // Indentation removed to avoid excessive scrolling
    // Fill in the list of users
    // TODO: Add more details to this list
    $sql = "SELECT id, uname, fullName from user ORDER BY uname";
    $sth = $db->prepare ($sql);
    $sth->execute ();
    while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
        echo '<tr>';
        echo "  <td><a href='mailto:{$row['uname']}'>{$row['uname']}</a></td><td>{$row['fullName']}</td><td><a href='javascript: deleteUser({$row['id']}, ".'"'.$row['fullName'].'"'.");' title='Slett denne brukeren'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></td>";
        echo '</tr>';
    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="container">
                <div class="jumbotron">
                    <h1>You are no administrator!</h1>
                    <p>Only administrators are allowed on this page</p>
                </div>
            </div>
        <?php }

        require_once 'include/bottomScriptIncludes.html';
    ?>
    <script src="../bower_components/dynatable/jquery.dynatable.js"></script>
    <script>
        $(function () { // Make the user table sortable, searchable and paginated
            $('#userTable').dynatable();
        });

        function deleteUser (id, name) {    // Avoid accidental removal of users
            var confirm = window.confirm('Er du sikker på at du vil slette brukeren ('+name+')');
            if (confirm)
                window.location = 'admin.php?deleteUser='+id;
        }
    </script>
  </body>
</html>
